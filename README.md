# READ ME 

Cette application a été développé dans le cadre du cours de technologie mobile dispensé à l'ENSAI par M. O. Levitt. 

Elle reprends le principe du jeu de carte [Card City Night](https://www.cardcitynights.com/).  

Les règles de ce jeu sont simple. Le but est de descendre les points de vie de son adversaire à zéro avant qu'il ne parvienne à nous vaincre. Pour se faire, chaque joueur dispose d'un deck de plusieurs carte : chaque carte a un effet particulier. Afin de pouvoir activer ces effets, les joueurs doivent effectuer un combo. 
En effet, chaque carte dispose d'une ou plusieurs flèches dans une des 8 directions de bases. Lorsque vous reliez deux ou trois cartes entre elles, c'est à dire lorsque vous les placez de sorte qu'au moins une des flèches qu'elles possèdent se rencontrent, vous déclenchez un combo. Un compte à rebours se lance alors ; l'effet des cartes s'effectue lorsque ce dernier est à 0. Mais ce n'est pas tout, certaines cartes ont des effets qui s'active dès qu'elles sont posées sur le plateau, voire même dès qu'elles sont piochées !

**Attention !** Il s'agit d'un jeu multijoueur, vous aurez donc besoin d'un ami pour pouvoir y jouer ! (Ou d'une deuxième fenêtre de navigateur, mais c'est moins drôle ...)

Une fois sur le menu du jeu, entrez le code : 

```
1:3;2:3;3:1;4:1;5:1;6:1;7:1;8:1;9:1;10:1;11:1;12:1;13:1;14:1;15:1;16:1;17:1;18:1;19:1;20:1
```

dans la partie "code du deck", puis cliquez sur le bouton "check code". Un message "Votre deck a bien été envoyé" va alors s'afficher. Vous pourrez ensuite cliquer sur "Jouer". Si le plateau ne s'affiche pas, rafraichir la page. 

Lorsqu'il sera à votre tour de jouer, il vous sera alors possible de cliquer sur les boutons "Jouer" en dessous des cartes de votre main, puis de choisir un emplacement sur le plateau où mettre la carte sélectionnée. Il est possible d'aggrandir les cartes en cliquant dessus. Une fois activées, les cartes se mettent en surbrillance. 

### Installation

Card City Night nécessite l'utilisation de [Node.js](https://nodejs.org/) pour fonctionner.

Installer les dépendances et démarrer le serveur : 

```sh
$ cd ccn
$ npm install -d
$ npm start
```

### Tech

Card City night a été développé à partir des outils suivants :  

* [React] 
* [Ionic]
* [node.js] 

### Developement

Vous voulez participer au projet ccn ? Super ! 
Envoyez un message à Maxence, il sera ravi ;) 

License
----

MIT


**Free Software, Hell Yeah!**