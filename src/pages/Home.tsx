import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonButton,
  IonButtons,
  IonImg,
  IonThumbnail,
  IonGrid,
  IonRow,
  IonCol,
  IonInput,
  IonToast,
} from "@ionic/react";
import React from "react";

import "./Home.css";
import IP from "../model/constants";
import { Redirect } from "react-router";

const Home: React.FC<{ match: any }> = ({ match }) => {
  const [text, setText] = React.useState<string>("");
  const [doRedirect, setRedirect] = React.useState<string | null>(null);
  const [showToast1, setShowToast1] = React.useState<boolean>(
    match.params.ended === "true"
  );
  const [showToast2, setShowToast2] = React.useState<boolean>(false);
  const [toast2, setToast2] = React.useState<string>("");
  const [showToast3, setShowToast3] = React.useState<boolean>(false);
  const [toast3, setToast3] = React.useState<string>("");

  const handleClickPlay = () => {
    fetch(`http://${IP}/play/`)
      .then((res) => res.json())
      .then((json) => {
        if (json.in_game) {
          setRedirect(json.ip);
          return;
        } else if (json.in_queue) {
          setToast2(json.message);
          setShowToast2(true);
          return setTimeout(handleClickPlay, 2000);
        } else {
          setToast2(json.message);
          setShowToast2(true);
        }
      });
  };

  const handleCheckDeck = () => {
    fetch(`http://${IP}/check-deck/${text}`)
      .then((res) => res.json())
      .then((json) => {
        if (json.response) {
          setToast3("Votre deck a bien été envoyé");
          setShowToast3(true);
        } else {
          setToast3(json.message);
          setShowToast3(true);
        }
      });
  };

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Card City Night 2</IonTitle>
          <IonButtons slot="end">
            <IonButton href="http://www.cardcitynights.com/" target="_blank">
              <IonThumbnail>
                <IonImg src="assets/icon/LogoRound.png" alt="CCN2" />
              </IonThumbnail>
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow className="main-row">
            <IonCol>
              <IonButton size="large" onClick={handleClickPlay}>
                Jouer
              </IonButton>
            </IonCol>
            <IonCol>
              <IonButton size="large" href="/deck-building">
                Créer un deck
              </IonButton>
            </IonCol>
          </IonRow>

          <IonRow className="secondary-row">
            <IonCol size="7">
              <IonInput
                value={text}
                placeholder="Code du Deck"
                onIonChange={(e) => setText(e.detail.value!)}
              ></IonInput>
            </IonCol>
            <IonCol size="5">
              <IonButton onClick={handleCheckDeck}>Check Code</IonButton>
            </IonCol>
          </IonRow>

          <IonRow className="secondary-row">
            <IonCol>
              <IonButton
                href="https://www.youtube.com/watch?v=i4hQ9veU0To"
                target="_blank"
              >
                Règle du jeu
              </IonButton>
            </IonCol>
            <IonCol>
              <IonButton href="http://www.cardcitynights.com/" target="_blank">
                Lien du jeu original
              </IonButton>
            </IonCol>
          </IonRow>
        </IonGrid>
        <IonToast
          isOpen={showToast1}
          onDidDismiss={() => setShowToast1(false)}
          message="Une erreur de connexion s'est produite ou l'un des deux joueurs a quitté la partie"
          duration={5000}
        />
        <IonToast
          isOpen={showToast2}
          onDidDismiss={() => setShowToast2(false)}
          message={toast2}
          duration={2000}
        />
        <IonToast
          isOpen={showToast3}
          onDidDismiss={() => setShowToast3(false)}
          message={toast3}
          duration={2000}
        />
        {doRedirect}
      </IonContent>
      {doRedirect ? (
        <Redirect to={"/boardgame/" + doRedirect}></Redirect>
      ) : null}
    </IonPage>
  );
};

export default Home;
