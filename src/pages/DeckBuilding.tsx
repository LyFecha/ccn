import React from "react";
import Card from "../components/Card";
import { IonContent } from "@ionic/react";
import Deck from "../components/Deck";
import defaultCard from "../model/defaultCard";

const DeckBuilding: React.FC = () => {
  return (
    <IonContent>
      <Card cardState={defaultCard}></Card>
      <Deck cards={[]} />
    </IonContent>
  );
};

export default DeckBuilding;
