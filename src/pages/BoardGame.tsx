import React from "react";
import Board from "../components/Board";
import Hand from "../components/Hand";
import { IonContent, IonGrid, IonCol, IonRow, IonItem } from "@ionic/react";
import Stats from "../components/StatsPlayer";
import Victory from "../components/Victory";
import IP from "../model/constants";
import { Redirect } from "react-router";
import Game from "../model/game";
import defaultGame from "../model/defaultGame";
import Historique from "../components/Historique";

const BoardGame: React.FC<any> = ({ match }) => {
  const ip = match.params.ip;
  const [phaseJeu, setPhaseJeu] = React.useState<boolean>(false);
  const [carteChoisie, setCarteChoisie] = React.useState<number>(-1);
  const [emplChoisi, setEmplChoisi] = React.useState<number>(-1);
  const [isPlayerTurn, setPlayerTurn] = React.useState<boolean>(false);
  const [game, setGame] = React.useState<Game>(defaultGame);
  const [victory, setVictory] = React.useState<boolean>(false);
  const [isBlue, setIsBlue] = React.useState<boolean>(false);
  const [doRedirect, setRedirect] = React.useState<boolean>(false);
  const [forcedRedirect, setForcedRedirect] = React.useState<boolean>(false);

  function handleclick(disabled: boolean) {
    setPhaseJeu(disabled);
  }

  function handleVictory(disabled: boolean) {
    setVictory(disabled);
    setRedirect(true);
  }

  function gameLoop() {
    if (!isPlayerTurn) {
      const fetchOtherPlayer = () =>
        fetch(`http://${IP}/other-player-turn/`)
          .then((res) => res.json())
          .then((json) => {
            console.log("Other-player-turn");
            console.log(json);
            if (json.in_game) {
              if (!json.other_player_turn) {
                setPlayerTurn(true);
                setIsBlue(ip === json.game.blueIP);
                setGame(json.game);
              } else {
                setTimeout(fetchOtherPlayer, 2000);
              }
            } else {
              setForcedRedirect(true);
              setRedirect(true);
            }
          })
          .catch(() => {
            setForcedRedirect(true);
            setRedirect(true);
          });
      fetchOtherPlayer();
    } else if (carteChoisie > -1 && emplChoisi > -1) {
      fetch(`http://${IP}/player-turn/${carteChoisie}/${emplChoisi}`)
        .then((res) => res.json())
        .then((json) => {
          console.log("Player-turn");
          console.log(json);
          if (json.in_game) {
            if (json.ok) {
              setPlayerTurn(false);
              setGame(json.game);
            }
          } else {
            setForcedRedirect(true);
            setRedirect(true);
          }
        })
        .catch(() => {
          setForcedRedirect(true);
          setRedirect(true);
        });
      setCarteChoisie(-1);
      setEmplChoisi(-1);
    }
  }

  return (
    <IonContent>
      {victory ? <Victory handleclick={handleVictory}></Victory> : victory}
      {isPlayerTurn ? (
        <IonItem>C'est à vous de jouer !</IonItem>
      ) : (
        <IonItem>En attente de l'adversaire..."</IonItem>
      )}
      <IonGrid>
        <IonRow>
          <IonCol>
            <Board
              handleclick={handleclick}
              choix={setEmplChoisi}
              jeu={phaseJeu}
              cards={game.card.field}
            />
          </IonCol>
          <IonCol size="2">
            <Stats
              player={"Votre adversaire"}
              pv={game[isBlue ? "redHP" : "blueHP"]}
              mana={game[isBlue ? "redToken" : "blueToken"].MANA}
              power={game[isBlue ? "redToken" : "blueToken"].POWER}
              deck={game.card[isBlue ? "redDeck" : "blueDeck"].length}
              shield={game[isBlue ? "redShield" : "blueShield"]}
            />
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol>
            <Hand
              handleclick={handleclick}
              turn={isPlayerTurn}
              choix={setCarteChoisie}
              card={game.card[!isBlue ? "redHand" : "blueHand"]}
            />
          </IonCol>
          <IonCol size="2">
            <Stats
              player={"Vous"}
              pv={game[!isBlue ? "redHP" : "blueHP"]}
              mana={game[!isBlue ? "redToken" : "blueToken"].MANA}
              power={game[!isBlue ? "redToken" : "blueToken"].POWER}
              deck={game.card[!isBlue ? "redDeck" : "blueDeck"].length}
              shield={game[!isBlue ? "redShield" : "blueShield"]}
            />
          </IonCol>
        </IonRow>
        <IonRow>
          <Historique historique={game.logs}></Historique>
        </IonRow>
      </IonGrid>
      {doRedirect ? (
        <Redirect to={forcedRedirect ? "/home/true" : "/home/false"}></Redirect>
      ) : null}
      {gameLoop()}
    </IonContent>
  );
};

export default BoardGame;
