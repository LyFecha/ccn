import React from "react";
import { IonButton, IonLabel, IonItem } from "@ionic/react";
import "./Board.tsx";

type handProps = {
  handleclick: Function; 
};

const Victory: React.FC<handProps> = ({handleclick}) => {

    function handleclickVictory() {
        handleclick(true);
      }

  return (
    <>
      <IonItem>
          <IonLabel>Félicitation, vous avez gagné !</IonLabel>
      </IonItem>   
      <IonItem>
          <IonButton size="small" href="/home" onClick={handleclickVictory}>Cliquez ici pour revenir au menu</IonButton>
      </IonItem>
    </>
  );
};

export default Victory;
