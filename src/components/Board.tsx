import React from "react";
import { IonGrid, IonRow } from "@ionic/react";
import cardState from "../model/cardState";
import Emplacement from "./Emplacement";

type boardProps = {
  handleclick: Function;
  choix: Function;
  jeu: boolean;
  cards: Array<cardState | null>;
};

const Board: React.FC<boardProps> = ({ handleclick, choix, jeu, cards }) => {
  const cardsOnGrid: Array<Array<cardState | null>> = [
    cards.slice(0, 6),
    cards.slice(6, 12),
    cards.slice(12, 18),
  ];
  return (
    <>
      <IonGrid>
        {cardsOnGrid.map((cardsRow, indexRow) => (
          <IonRow key={indexRow} className="ion-align-items-center">
            {cardsRow.map((card, index) => (
              <Emplacement
                key={index}
                handleclick={handleclick}
                choix={choix}
                jeu={jeu}
                card={card}
                idEmpl={index + indexRow * 6}
              />
            ))}
          </IonRow>
        ))}
      </IonGrid>
    </>
  );
};

export default Board;
