import React, { useState } from "react";
import {
  IonCard,
  IonImg,
  IonModal,
  IonGrid,
  IonRow,
  IonCol,
  IonLabel,
} from "@ionic/react";
import cardProps from "../model/cardProps";
import IP from "../model/constants";
import Color from "../model/color";
import Base from "../model/base";
import cardState from "../model/cardState";

const Card: React.FC<cardProps> = (props: cardProps) => {
  const card = props.cardState;

  const [showModal, setShowModal] = useState(false);

  const [currentStatus, setCurrentStatus] = React.useState<string>();

  /*  NONE = 0,
  CAGED = 1,
  SILENCED = 1 << 1,
  NON_CONDUCTIVE = 1 << 2,
  ACTIVATED = 1 << 3, */

  function tradStatus(card: cardState) {
    if (card.status === 0) {
      setCurrentStatus("None");
    } else if (card.status === 1) {
      setCurrentStatus("Caged");
    } else if (card.status === 2) {
      setCurrentStatus("Silenced");
    } else if (card.status === 8) {
      setCurrentStatus("Activated");
    }
    console.log(card.status);
  }

  function handleclickCardInGame(card: cardState) {
    if (card.cardId !== -1) {
      setShowModal(true);
      tradStatus(card);
    }
  }

  const imgSrc = `http://${IP}/cardImg/${card.name}/${card.arrows}/${
    card.delay
  }/${Base[card.base]}/${card.description}/${Color[String(card.isBlue)]}/${
    Color[String(card.isArrowBlue)]
  }/${card.status}`;

  return (
    <>
      <IonCard
        button={true}
        onClick={() => {
          handleclickCardInGame(card);
        }}
      >
        <IonImg src={imgSrc} alt={`Carte ${card.name}`}></IonImg>
      </IonCard>

      <IonModal isOpen={showModal} onDidDismiss={() => setShowModal(false)}>
        <IonGrid>
          <IonRow>
            <IonCol>
              <IonImg
                src={imgSrc}
                alt="Carte en main 1"
                onClick={() => setShowModal(false)}
              ></IonImg>
            </IonCol>
            <IonCol>
              <IonRow>
                <IonLabel color="primary">{card.description}</IonLabel>
              </IonRow>
              <IonRow>
                <IonLabel color="primary">
                  Status de cette carte : {currentStatus}
                </IonLabel>
              </IonRow>
              <IonRow>
                <IonLabel color="primary">
                  Cliquez sur la carte pour revenir
                </IonLabel>
              </IonRow>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonModal>
    </>
  );
};

export default Card;
