import React from "react";
import { IonItem, IonList, IonContent, IonLabel } from "@ionic/react";

type historiqueProps = {
  historique: Array<String>;
};

const Historique: React.FC<historiqueProps> = ({ historique }) => {
  console.log(historique);

  return (
    <IonContent>
      <IonList>
        {historique.map((log, index) => (
          <IonItem key={index}>
            <IonLabel>{log}</IonLabel>
          </IonItem>
        ))}
      </IonList>
    </IonContent>
  );
};

export default Historique;
