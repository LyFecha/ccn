import React from "react";
import { IonGrid, IonCol, IonRow, IonButton } from "@ionic/react";
import "./Board.tsx";
import cardState from "../model/cardState";
import Card from "./Card";

type handProps = {
  handleclick: Function;
  choix: Function;
  turn: boolean;
  card: Array<cardState>;
};

const Hand: React.FC<handProps> = ({ handleclick, choix, turn, card }) => {
  function handleclickHand(idHand: Number) {
    choix(idHand);
    handleclick(true);
  }

  return (
    <>
      <IonGrid>
        <IonRow className="ion-align-items-center">
          {card.map((oneCard, index) => (
            <IonCol key={index}>
              <Card key={`${index}_Card`} cardState={oneCard} />
              <IonButton
                key={`${index}_Button`}
                disabled={!turn}
                class="default"
                onClick={() => handleclickHand(index)}
              >
                Jouer
              </IonButton>
            </IonCol>
          ))}
        </IonRow>
      </IonGrid>
    </>
  );
};

export default Hand;
