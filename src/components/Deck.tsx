import React from "react";
import { IonGrid, IonCol, IonRow } from "@ionic/react";

type deckProps = {
  cards: Array<React.FC>;
};

const Deck: React.FC<deckProps> = ({ cards }) => {
  return (
    <IonGrid>
      <IonRow>
        {cards.map((card) => (
          <IonCol>{card}</IonCol>
        ))}
      </IonRow>
    </IonGrid>
  );
};

export default Deck;
