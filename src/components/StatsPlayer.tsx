import React from "react";
import { IonGrid, IonRow, IonItem } from "@ionic/react";

type statsProps = {
  player: string;
  pv: number;
  mana: number;
  power: number;
  deck: number;
  shield: boolean;
};

const Stats: React.FC<statsProps> = ({ player, pv, mana, power, deck, shield }) => {
  return (
    <IonGrid>
      <IonRow>
        <IonItem>{player}</IonItem>
      </IonRow>
      <IonRow>
        <IonItem>
          PV: {pv}
          {shield ? " (Bouclier)" : ""}
        </IonItem>
      </IonRow>
      {mana > 0 ? (
        <IonRow>
          <IonItem>Mana : {mana} </IonItem>
        </IonRow>
      ) : null}
      {mana > 0 ? (
        <IonRow>
          <IonItem>Power : {power} </IonItem>
        </IonRow>
      ) : null}
      <IonRow>
        <IonItem>Deck : {deck} cartes </IonItem>
      </IonRow>
    </IonGrid>
  );
};

export default Stats;
