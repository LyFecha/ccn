import React from "react";
import { IonCol, IonCard, IonImg } from "@ionic/react";
import cardState from "../model/cardState";
import Status from "../model/status";
import Card from "./Card";

type emplacementProps = {
  handleclick: Function;
  choix: Function;
  jeu: boolean;
  card: cardState | null;
  idEmpl: number;
};

//prettier-ignore
const Emplacement: React.FC<emplacementProps> = ({handleclick, choix, jeu, card, idEmpl}) => {

    const isActivated : boolean = (card !== null && (card.status & Status.ACTIVATED) === Status.ACTIVATED);  

    function handleclickEmplacement (idEmpl : Number, card : cardState | null) {
        if(jeu && !card) {choix(idEmpl);
            handleclick(false);}
      };  

    return (
        <>   
            {card ? 
                (isActivated ?
                    <IonCol>
                        <IonCard button={jeu} onClick={() => {handleclickEmplacement(idEmpl, card);}}  color="success">
                            <Card cardState={card} />
                        </IonCard>
                    </IonCol>   
                : 
                    <IonCol>
                        <IonCard button={jeu} onClick={() => {handleclickEmplacement(idEmpl, card);}}>
                            <Card cardState={card} />
                        </IonCard>
                    </IonCol>   
                )
                :
                <IonCol>
                    <IonCard button={jeu} onClick={() => {handleclickEmplacement(idEmpl, card);}}>
                        <IonImg src="../../assets/Board.png" />
                    </IonCard>
                </IonCol>   
            }
        </>
    );
};

export default Emplacement;
