import cardState from "./cardState";

type cardProps = {
  cardState: cardState;
};

export default cardProps;
