import Game from "./game";
import cardState from "./cardState";

const defaultGame: Game = {
  blueIP: "0.0.0.0",
  redIP: "0.0.0.0",
  blueHP: 10,
  redHP: 10,
  blueShield: false,
  redShield: false,
  blueToken: { MANA: 0, POWER: 0, FATIGUE: 0 },
  redToken: { MANA: 0, POWER: 0, FATIGUE: 0 },
  blueTurn: true,
  endGame: false,
  card: {
    blueDeck: [],
    redDeck: [],
    blueHand: [],
    redHand: [],
    field: (Array(18) as Array<cardState | null>).fill(null),
  },
  logs: [],
};

export default defaultGame;
