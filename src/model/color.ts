export const Color: { [index: string]: string } = {
  false: "RED",
  true: "BLUE",
};

export default Color;
