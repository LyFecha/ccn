import cardState from "./cardState";
import Base from "./base";
import Status from "./status";

export const defaultCard: cardState = {
  cardId: 0,
  base: Base.COMMON,
  name: "Key",
  arrows: 0,
  delay: 3,
  description: "Ce n'est pas un placeholder !",
  image: "../public/assets/Apathetic Frog.png",
  status: Status.NONE,
  isBlue: true,
  isArrowBlue: true,
};

export default defaultCard;
