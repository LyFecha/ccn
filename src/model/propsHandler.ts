type propsHandler = {
  onPlay?: Function;
  onDeckBuilding?: Function;
  onHome?: Function;
};

export default propsHandler;
