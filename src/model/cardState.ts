import Status from "./status";
import Base from "./base";
import CardsId from "./cardsId";

type cardState = {
  cardId: CardsId;
  base: Base;
  name: string;
  arrows: number;
  delay: number;
  description: string;
  image?: string;
  status: Status;
  isBlue: boolean;
  isArrowBlue: boolean;
};

export default cardState;
