import cardState from "./cardState";
import Base from "./base";
import CardsId from "./cardsId";
import Status from "./status";

// NE PAS ENLEVER :
// prettier-ignore
const Cards : {[index: string] : cardState} = {
  NULL: {cardId: CardsId.NULL, name: "Null", arrows: 0b00000000, delay: 1, base: Base["COMMON"], description: "", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  // Obtainable
  //1
  ADWOA: {cardId: CardsId.ADWOA, name: "Adwoa", arrows: 0b00100000, delay: 3, base: Base["COMMON"], description: ":NC: :n: :R: heal 1 & deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  ALEXANDER: {cardId: CardsId.ALEXANDER, name: "Alexander", arrows: 0b10101000, delay: 3, base: Base["COMMON"], description: ":R: deal 2 damage.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  ALF: {cardId: CardsId.ALF, name: "Alf", arrows: 0b00000010, delay: 3, base: Base["COMMON"], description: ":P: peek at your opponent's top 3 deck cards. :n: :R: heal 1.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  ALPHA_STRIKE: {cardId: CardsId.ALPHA_STRIKE, name: "Alpha Strike", arrows: 0b00100000, delay: 2, base: Base["RARE"], description: ":R: Remove all cards except one chosen at random.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  ANCIENT_WAR: {cardId: CardsId.ANCIENT_WAR, name: "Ancient War", arrows: 0b00000000, delay: 3, base: Base["RARE"], description: ":P: activate this card. :n: When a card is played: transform it into µrandom µTurnip µcard.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  ANCIENT_WAR_X: {cardId: CardsId.ANCIENT_WAR_X, name: "Ancient War X", arrows: 0b00000000, delay: 3, base: Base["RARE"], description: ":P: activate this card. :n: When a card is played: transform it into µrandom µJenny µcard.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  ANTI_MURI: {cardId: CardsId.ANTI_MURI, name: "Anti-Muri", arrows: 0b10101000, delay: 3, base: Base["UNCOMMON"], description: ":P: take 3 damage. :n: :R: deal 3 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  APATHETIC_FROG: {cardId: CardsId.APATHETIC_FROG, name: "Apathetic Frog", arrows: 0b00000000, delay: 6, base: Base["COMMON"], description: "", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //2
  APATHETIC_FROG_X: {cardId: CardsId.APATHETIC_FROG_X, name: "Apathetic Frog X", arrows: 0b00000000, delay: 6, base: Base["RARE"], description: "When you play a £Apathetic card: add 2 £Apathetic £Frog cards to your opponent's deck.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  APATHETIC_JENNY: {cardId: CardsId.APATHETIC_JENNY, name: "Apathetic Jenny", arrows: 0b01000000, delay: 5, base: Base["COMMON"], description: ":R: add 2 £Apathetic £Frog cards to your opponent's deck.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  ASHA: {cardId: CardsId.ASHA, name: "Asha", arrows: 0b10010000, delay: 2, base: Base["UNCOMMON"], description: ":P: move this card to a random space & activate this card. :n: :R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  AYA: {cardId: CardsId.AYA, name: "Aya", arrows: 0b00110100, delay: 3, base: Base["COMMON"], description: ":R: peek at your top 3 deck cards.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BAZOOKA_TURNIP: {cardId: CardsId.BAZOOKA_TURNIP, name: "Bazooka Turnip", arrows: 0b10000000, delay: 6, base: Base["UNCOMMON"], description: ":P: gain shield. :n: :R: deal 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BEAR_MINER: {cardId: CardsId.BEAR_MINER, name: "Bear Miner", arrows: 0b10001101, delay: 2, base: Base["COMMON"], description: "When active & your opponent plays a card: move this card to a random space.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BEAUTIFUL_CREATURES: {cardId: CardsId.BEAUTIFUL_CREATURES, name: "Beautiful Creatures", arrows: 0b11111111, delay: 1, base: Base["COMMON"], description: ":P: take 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BEE_SWARM: {cardId: CardsId.BEE_SWARM, name: "Bee Swarm", arrows: 0b01000101, delay: 3, base: Base["COMMON"], description: ":R: the player with the least life heals 1.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //3
  BEES: {cardId: CardsId.BEES, name: "Bees", arrows: 0b11000000, delay: 2, base: Base["UNCOMMON"], description: ":NC: :n: When active & a card is played: silence it.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BEES_X: {cardId: CardsId.BEES_X, name: "Bees X", arrows: 0b01100000, delay: 2, base: Base["UNCOMMON"], description: ":NC: :n: When active & a card is played: cage it.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BENNY: {cardId: CardsId.BENNY, name: "Benny", arrows: 0b10111011, delay: 1, base: Base["UNCOMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BIRD: {cardId: CardsId.BIRD, name: "Bird", arrows: 0b00010000, delay: 2, base: Base["COMMON"], description: ":P: add a £Bird card without this ability to the top of your deck.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BIRD_ARROW: {cardId: CardsId.BIRD_ARROW, name: "Bird Arrow", arrows: 0b01000001, delay: 1, base: Base["COMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BLUEPRINT: {cardId: CardsId.BLUEPRINT, name: "Blueprint", arrows: 0b10101010, delay: 3, base: Base["RARE"], description: ":P: activate this card. :n: :R: add a £Blueprint card without this ability to your deck.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BOB_S_SHIP: {cardId: CardsId.BOB_S_SHIP, name: "Bob's Ship", arrows: 0b11111111, delay: 2, base: Base["UNCOMMON"], description: "", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BOMB: {cardId: CardsId.BOMB, name: "Bomb", arrows: 0b00000000, delay: 1, base: Base["COMMON"], description: ":D: reveal this card & take 1 damage & discard this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //4
  BOSS_TRIO: {cardId: CardsId.BOSS_TRIO, name: "Boss Trio", arrows: 0b10110101, delay: 1, base: Base["UNCOMMON"], description: ":R: deal 3 damage to the player with the most life.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BRICKOPALYPSE: {cardId: CardsId.BRICKOPALYPSE, name: "Brickopalypse", arrows: 0b10100100, delay: 8, base: Base["COMMON"], description: ":NC: :n: :R: deal 4 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BRUTUS: {cardId: CardsId.BRUTUS, name: "Brutus", arrows: 0b00000011, delay: 6, base: Base["UNCOMMON"], description: ":R: deal 1 damage for each free adjacent space on the board.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BUNBOY: {cardId: CardsId.BUNBOY, name: "Bunboy", arrows: 0b11011011, delay: 3, base: Base["RARE"], description: ":D: reveal this car & deal 2 damage to the player with the most life.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BUNI: {cardId: CardsId.BUNI, name: "Buni", arrows: 0b01000010, delay: 2, base: Base["COMMON"], description: ":NC: :n: :R: gain shield & discard a card from your hand.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BUSINESS_CASUAL_MAN: {cardId: CardsId.BUSINESS_CASUAL_MAN, name: "Business Casual Man", arrows: 0b10100100, delay: 3, base: Base["RARE"], description: "When active: whenever you would deal damage to yourself; deal 1 damage instead.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BUSINESS_CASUAL_MAN__X: {cardId: CardsId.BUSINESS_CASUAL_MAN__X, name: "Business Casual Man X", arrows: 0b00000000, delay: 4, base: Base["RARE"], description: "When active: whenever you would deal damage to yourself; deal 1 damage instead.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  CANDY_SNAKE: {cardId: CardsId.CANDY_SNAKE, name: "Candy Snake", arrows: 0b01011001, delay: 4, base: Base["UNCOMMON"], description: "", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //5
  CARDPLAYA: {cardId: CardsId.CARDPLAYA, name: "CardPlaya", arrows: 0b10000100, delay: 2, base: Base["UNCOMMON"], description: ":R: switch the color of an opponent card & switch the color of one of your cards.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  CASUAL_JENNY_BERRY: {cardId: CardsId.CASUAL_JENNY_BERRY, name: "Casual Jenny Berry", arrows: 0b00001000, delay: 1, base: Base["COMMON"], description: ":P: shuffle the cards in your hand into your deck.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  CAUTION: {cardId: CardsId.CAUTION, name: "Caution", arrows: 0b00011000, delay: 2, base: Base["COMMON"], description: ":R: players can't take damage until your next turn.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  CENTURION_TURNIP: {cardId: CardsId.CENTURION_TURNIP, name: "Centurion Turnip", arrows: 0b00001001, delay: 2, base: Base["COMMON"], description: ":R: deal 1 damage. :n: When you play a £Turnip card: transform this card into µBazooka µTurnip.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  CHILLY_ROGER: {cardId: CardsId.CHILLY_ROGER, name: "Chilly Roger", arrows: 0b00000000, delay: 1, base: Base["UNCOMMON"], description: ":P: activate this card. :n: :R: you can't take damage until your next turn.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  COGS: {cardId: CardsId.COGS, name: "Cogs", arrows: 0b00011110, delay: 2, base: Base["COMMON"], description: ":P: add a £random card to the top of your deck.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  CORRUPTION: {cardId: CardsId.CORRUPTION, name: "Corruption", arrows: 0b00000001, delay: 2, base: Base["COMMON"], description: ":R: deal 1 damage & delay an active card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  CROSSOVER_ADVENTURE: {cardId: CardsId.CROSSOVER_ADVENTURE, name: "Crossover Adventure", arrows: 0b10101011, delay: 3, base: Base["UNCOMMON"], description: ":NC: :n: :R: deal 1 damage & heal 1 & take 1 damage & gain shield.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //6
  CRUISER_TETRON: {cardId: CardsId.CRUISER_TETRON, name: "Cruiser Tetron", arrows: 0b11101000, delay: 3, base: Base["UNCOMMON"], description: ":NC: :n: :R: deal 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  CYBER_JENNY: {cardId: CardsId.CYBER_JENNY, name: "Cyber Jenny", arrows: 0b11111111, delay: 3, base: Base["RARE"], description: ":D: reveal this card & deal 1 damage. :n: :P: deal 1 damage. :n: :R: deal 1 damage.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  CYBER_SKULL: {cardId: CardsId.CYBER_SKULL, name: "Cyber Skull", arrows: 0b11011111, delay: 1, base: Base["UNCOMMON"], description: ":R: take 2 damage & deal 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  DANCE_PARTY: {cardId: CardsId.DANCE_PARTY, name: "Dance party", arrows: 0b11010010, delay: 3, base: Base["RARE"], description: ":R: activate a card & activate a card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  DARK_LORD: {cardId: CardsId.DARK_LORD, name: "Dark Lord", arrows: 0b01100000, delay: 2, base: Base["UNCOMMON"], description: ":R: switch the color of one of your opponent's inactive cards.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  DARK_LORD_X: {cardId: CardsId.DARK_LORD_X, name: "Dark Lord X", arrows: 0b01100000, delay: 2, base: Base["RARE"], description: ":NC: :n: :R: switch the color of an opponent card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  DARK_RUBY: {cardId: CardsId.DARK_RUBY, name: "Dark Ruby", arrows: 0b01000011, delay: 1, base: Base["UNCOMMON"], description: ":R: pop your opponent's shield. If you did: deal 1 damage & gain shield.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  DEA: {cardId: CardsId.DEA, name: "Dea", arrows: 0b00001010, delay: 3, base: Base["COMMON"], description: ":P: pay a MANA token to transform this card into µNeo µDea & peek at your opponent's hand.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //7
  DEATHBLOW: {cardId: CardsId.DEATHBLOW, name: "Deathblow", arrows: 0b00111110, delay: 1, base: Base["RARE"], description: ":R: pop both player's shields. If you did: deal 5 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  DENTIST_MAZE: {cardId: CardsId.DENTIST_MAZE, name: "Dentist Maze", arrows: 0b00001100, delay: 2, base: Base["UNCOMMON"], description: ":NC: :n: When your opponent plays a card: move it to a random space & activate this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  DEVASOM: {cardId: CardsId.DEVASOM, name: "Devasom", arrows: 0b10010100, delay: 2, base: Base["UNCOMMON"], description: ":P: silence a common card. :n: :R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  DEVIL_REMEDY: {cardId: CardsId.DEVIL_REMEDY, name: "Devil Remedy", arrows: 0b00000100, delay: 4, base: Base["UNCOMMON"], description: ":P: steam a MANA token from your opponent. :n: :R: deal 1 damage & heal 1.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  DISILLUSIONED_SNAKE: {cardId: CardsId.DISILLUSIONED_SNAKE, name: "Disillusioned Snake", arrows: 0b00100010, delay: 3, base: Base["COMMON"], description: ":P: delay all active cards.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  DOWNHILL_DUCK: {cardId: CardsId.DOWNHILL_DUCK, name: "Downhill Duck", arrows: 0b01110000, delay: 3, base: Base["COMMON"], description: ":P: tick all active cards. :n: :R: take 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  DREAM_ROCK: {cardId: CardsId.DREAM_ROCK, name: "Dream Rock", arrows: 0b00000000, delay: 1, base: Base["COMMON"], description: ":D: reveal this card and pay 2 MANA tokens to gain 3 MANA tokens.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  DRONE: {cardId: CardsId.DRONE, name: "Drone", arrows: 0b11100000, delay: 2, base: Base["COMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //8
  DUCKLORD: {cardId: CardsId.DUCKLORD, name: "Ducklord", arrows: 0b01010010, delay: 2, base: Base["COMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  DUMPED: {cardId: CardsId.DUMPED, name: "Dumped", arrows: 0b11100000, delay: 2, base: Base["COMMON"], description: ":NC: :n: :R: remove an active card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  DUMPED_X: {cardId: CardsId.DUMPED_X, name: "Dumped X", arrows: 0b01000000, delay: 1, base: Base["UNCOMMON"], description: ":R: remove a card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  DYNAMITE: {cardId: CardsId.DYNAMITE, name: "Dynamite", arrows: 0b00000001, delay: 3, base: Base["COMMON"], description: ":P: activate this card :n: :R: deal 1 damage & take 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  EVIL_EASEL: {cardId: CardsId.EVIL_EASEL, name: "Evil Easel", arrows: 0b11000111, delay: 1, base: Base["UNCOMMON"], description: ":P: silence an adjacent legendary card. :n: :R: deal 1 damage to the player with the most life.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  FAE: {cardId: CardsId.FAE, name: "Fae", arrows: 0b01101000, delay: 3, base: Base["UNCOMMON"], description: "When active and a £Tippsie card is played: remove it & heal 2.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  FASEN: {cardId: CardsId.FASEN, name: "Fasen", arrows: 0b10001000, delay: 3, base: Base["UNCOMMON"], description: ":R: add 2 £Potato cards to your deck.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  FEAR_OF_BIRDS: {cardId: CardsId.FEAR_OF_BIRDS, name: "Fear of Birds", arrows: 0b00000000, delay: 4, base: Base["UNCOMMON"], description: "When active & your opponent plays a £bird card: take 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //9
  FERAL_GATE: {cardId: CardsId.FERAL_GATE, name: "Feral Gate", arrows: 0b00010010, delay: 2, base: Base["COMMON"], description: ":P: cage an adjacent card at random. :n: :R: restore an adjacent card at random.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  FINAL_BRUTUS: {cardId: CardsId.FINAL_BRUTUS, name: "Final Brutus", arrows: 0b01100101, delay: 4, base: Base["UNCOMMON"], description: ":NC: :n: :R: deal 2 piercing damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  FIRE_MACE: {cardId: CardsId.FIRE_MACE, name: "Fire Mace", arrows: 0b01000000, delay: 3, base: Base["UNCOMMON"], description: ":P: deal 1 damage. :n: :R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  FIRE_SWORD: {cardId: CardsId.FIRE_SWORD, name: "Fire Sword", arrows: 0b01100000, delay: 3, base: Base["COMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  FISHBUN: {cardId: CardsId.FISHBUN, name: "Fishbun", arrows: 0b10001000, delay: 3, base: Base["COMMON"], description: "When a £Bun card is played: remove it & transform this card into µFishbun µPileup.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  FLIP_HERO: {cardId: CardsId.FLIP_HERO, name: "Flip Hero", arrows: 0b00100010, delay: 3, base: Base["COMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  FORCE_WAND: {cardId: CardsId.FORCE_WAND, name: "Force Wand", arrows: 0b00010001, delay: 1, base: Base["UNCOMMON"], description: ":P: gain 1 MANA token. :n: :R: deal a damage for every 2 MANA tokens you have.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  FRALLAN: {cardId: CardsId.FRALLAN, name: "Frallan", arrows: 0b10100110, delay: 3, base: Base["UNCOMMON"], description: ":NC: :n: When active: Whenever you would heal 1; heal 2 instead.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //10
  FRIEND: {cardId: CardsId.FRIEND, name: "Friend", arrows: 0b00101001, delay: 3, base: Base["UNCOMMON"], description: ":R: activate a card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  FROG_FAN: {cardId: CardsId.FROG_FAN, name: "Frog Fan", arrows: 0b01100010, delay: 3, base: Base["COMMON"], description: ":R: discard a card from you hand.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  GARDEN_GNOME: {cardId: CardsId.GARDEN_GNOME, name: "Garden Gnome", arrows: 0b11111111, delay: 3, base: Base["COMMON"], description: ":NC:", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  GODDESS_BUSTER: {cardId: CardsId.GODDESS_BUSTER, name: "Goddess Buster", arrows: 0b10001111, delay: 2, base: Base["UNCOMMON"], description: ":R: deal 1 damage. When you play a £Goddess £of £Explosions card: deal 2 damage & remove this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  GODDESS_OF_EXPLOSIONS: {cardId: CardsId.GODDESS_OF_EXPLOSIONS, name: "Goddess of Explosions", arrows: 0b10001111, delay: 1, base: Base["UNCOMMON"], description: ":P: take 1 damage & deal 1 damage. :n: :R: take 1 damage & deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  GOLDEN_APATHETIC_FROG: {cardId: CardsId.GOLDEN_APATHETIC_FROG, name: "Golden Apathetic Frog", arrows: 0b00000000, delay: 6, base: Base["UNCOMMON"], description: "When active & your opponent plays a £Apathetic £Frog card: win the game.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  GUNNEL_VISION: {cardId: CardsId.GUNNEL_VISION, name: "Gunnel Vision", arrows: 0b11111111, delay: 1, base: Base["COMMON"], description: "When you play a £Wand card: deal 1 damage.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  HAJA: {cardId: CardsId.HAJA, name: "Haja", arrows: 0b00000110, delay: 4, base: Base["COMMON"], description: ":NC: :n: :P: take 1 damage. :n: :R: deal 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //11
  HANDS: {cardId: CardsId.HANDS, name: "Hands", arrows: 0b01010101, delay: 2, base: Base["UNCOMMON"], description: ":NC: :n: :R: cage a card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  HEART: {cardId: CardsId.HEART, name: "Heart", arrows: 0b01010101, delay: 1, base: Base["COMMON"], description: ":D: reveal this card & heal 1.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  HEDVIG: {cardId: CardsId.HEDVIG, name: "Hedvig", arrows: 0b11011000, delay: 1, base: Base["UNCOMMON"], description: ":P: activate an opponent card. :n: :R: activate one of your cards.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  HELIOS: {cardId: CardsId.HELIOS, name: "Helios", arrows: 0b00110000, delay: 2, base: Base["UNCOMMON"], description: "When active and your opponent plays a card: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  HERMITLEGS: {cardId: CardsId.HERMITLEGS, name: "HermitLegs", arrows: 0b11000001, delay: 2, base: Base["RARE"], description: ":R: move all cards to a random space.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  HEROISM: {cardId: CardsId.HEROISM, name: "Heroism", arrows: 0b10101100, delay: 1, base: Base["COMMON"], description: "When your opponent plays a £Jenny card: deal 1 damage & silence this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  HEROISM_X: {cardId: CardsId.HEROISM_X, name: "Heroism X", arrows: 0b10101100, delay: 1, base: Base["COMMON"], description: "When your opponent plays a £Ittle card: deal 1 damage & silence this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  HEXROT: {cardId: CardsId.HEXROT, name: "Hexrot", arrows: 0b11011000, delay: 3, base: Base["RARE"], description: ":R: deal a damage for every MANA token you have.", isBlue: true, isArrowBlue: false, status: Status.NONE,},

  //12
  HEXROT_X: {cardId: CardsId.HEXROT_X, name: "Hexrot X", arrows: 0b11011000, delay: 4, base: Base["UNCOMMON"], description: ":R: take a damage for every MANA token you have.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  HORSEGUN: {cardId: CardsId.HORSEGUN, name: "Horsegun", arrows: 0b10110001, delay: 2, base: Base["COMMON"], description: ":R: add a £Horsegun card without this ability to your deck. :n: When you play a £Horse card: deal 1 damage.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  HYPE_SNAKE: {cardId: CardsId.HYPE_SNAKE, name: "Hype Snake", arrows: 0b01100110, delay: 4, base: Base["RARE"], description: "When a card is played: move this card to a random space & activate this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  HYPERDUSA: {cardId: CardsId.HYPERDUSA, name: "Hyperdusa", arrows: 0b01010101, delay: 2, base: Base["UNCOMMON"], description: ":P: deal 1 damage to the player with the most life. :n: :R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  ICE_RING: {cardId: CardsId.ICE_RING, name: "Ice Ring", arrows: 0b00100010, delay: 2, base: Base["COMMON"], description: ":P: cage an active card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  ICE_WAND: {cardId: CardsId.ICE_WAND, name: "Ice Wand", arrows: 0b01000100, delay: 2, base: Base["COMMON"], description: ":P: delay an active card. :n: :R: delay an active card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  IDUN: {cardId: CardsId.IDUN, name: "Idun", arrows: 0b01101000, delay: 3, base: Base["UNCOMMON"], description: "When you play a £Idun card: silence a card & cage a card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  IJI: {cardId: CardsId.IJI, name: "Iji", arrows: 0b11110001, delay: 5, base: Base["UNCOMMON"], description: ":U: :n: :P: opponent gains shield. :n: :R: gain shield.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //13
  INCREDIBLY_UGLY_STATUES: {cardId: CardsId.INCREDIBLY_UGLY_STATUES, name: "Incredibly Ugly Statues", arrows: 0b00000000, delay: 1, base: Base["COMMON"], description: ":U:", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  IOSA: {cardId: CardsId.IOSA, name: "Iosa", arrows: 0b10011000, delay: 3, base: Base["UNCOMMON"], description: ":NC: :n: :U: :n: :R: deal 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  ITAN_CARVER: {cardId: CardsId.ITAN_CARVER, name: "Itan Carver", arrows: 0b10010010, delay: 2, base: Base["COMMON"], description: ":P: add a £Bomb card to your opponent's deck.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  ITTLE_DEW: {cardId: CardsId.ITTLE_DEW, name: "Ittle Dew", arrows: 0b10010100, delay: 1, base: Base["COMMON"], description: ":R: deal 2 damage & take 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  ITTLE_DEW_TOO: {cardId: CardsId.ITTLE_DEW_TOO, name: "Ittle Dew Too", arrows: 0b11000000, delay: 4, base: Base["COMMON"], description: ":NC: :n: :R: deal 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  JAM: {cardId: CardsId.JAM, name: "Jam", arrows: 0b00101100, delay: 2, base: Base["UNCOMMON"], description: ":NC: :n: :R: heal 2.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  JEALOUS_CHEST: {cardId: CardsId.JEALOUS_CHEST, name: "Jealous Chest", arrows: 0b00000000, delay: 4, base: Base["RARE"], description: ":R: gain a POWER token.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  JELLY: {cardId: CardsId.JELLY, name: "Jelly", arrows: 0b00000000, delay: 2, base: Base["UNCOMMON"], description: ":P: activate this card. :n: :R: silence an adjacent card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //14
  JENNY_BIRD: {cardId: CardsId.JENNY_BIRD, name: "Jenny Bird", arrows: 0b00000000, delay: 1, base: Base["UNCOMMON"], description: "When you play a £Bird card: transform it into µBird µArrow & activate it.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  JENNY_BIRD_X: {cardId: CardsId.JENNY_BIRD_X, name: "Jenny Bird X", arrows: 0b00000000, delay: 1, base: Base["UNCOMMON"], description: "When you play a £Bird £Arrow card: transform it into µBird & activate it.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  JENNY_BLUEBERRY: {cardId: CardsId.JENNY_BLUEBERRY, name: "Jenny Blueberry", arrows: 0b01010101, delay: 3, base: Base["COMMON"], description: ":NC: :n: :P: activate this card. :R: deal 1 damage & take 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  JENNY_BUN: {cardId: CardsId.JENNY_BUN, name: "Jenny Bun", arrows: 0b00100000, delay: 3, base: Base["COMMON"], description: "When active & a £Bun card is played: remove it & gain shield.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  JENNY_BUNNY: {cardId: CardsId.JENNY_BUNNY, name: "Jenny Bunny", arrows: 0b00100100, delay: 2, base: Base["UNCOMMON"], description: "When your opponent plays a card: remove it & silence this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  JENNY_BUNNY_X: {cardId: CardsId.JENNY_BUNNY_X, name: "Jenny Bunny X", arrows: 0b00100100, delay: 2, base: Base["RARE"], description: "When your opponent plays a card: switch the color of it & silence this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  JENNY_CAT: {cardId: CardsId.JENNY_CAT, name: "Jenny Cat", arrows: 0b01101010, delay: 2, base: Base["COMMON"], description: ":NC: :n: :R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  JENNY_DEER: {cardId: CardsId.JENNY_DEER, name: "Jenny Deer", arrows: 0b00100000, delay: 2, base: Base["RARE"], description: ":R: remove 3 MANA tokens & deal 2 damage for every MANA token you have.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //15
  JENNY_FLOWER: {cardId: CardsId.JENNY_FLOWER, name: "Jenny Flower", arrows: 0b10000010, delay: 1, base: Base["UNCOMMON"], description: ":P: heal 1. :n: :R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  JENNY_FOX: {cardId: CardsId.JENNY_FOX, name: "Jenny Fox", arrows: 0b00000101, delay: 1, base: Base["UNCOMMON"], description: ":P: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  JENNY_FROG: {cardId: CardsId.JENNY_FROG, name: "Jenny Frog", arrows: 0b10100000, delay: 2, base: Base["COMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  JENNY_LEMON: {cardId: CardsId.JENNY_LEMON, name: "Jenny Lemon", arrows: 0b00100010, delay: 5, base: Base["RARE"], description: "When active & a £Jenny card is played: remove it & deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  JENNY_MERMAID: {cardId: CardsId.JENNY_MERMAID, name: "Jenny Mermaid", arrows: 0b01101100, delay: 3, base: Base["COMMON"], description: ":NC: :n: :R: deal 1 damage.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  JENNY_MOLE: {cardId: CardsId.JENNY_MOLE, name: "Jenny Mole", arrows: 0b01110000, delay: 2, base: Base["COMMON"], description: ":U: :n: :R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  JENNY_SHARK: {cardId: CardsId.JENNY_SHARK, name: "Jenny Shark", arrows: 0b10000001, delay: 3, base: Base["UNCOMMON"], description: ":R: deal 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  JENNY_SLAYER: {cardId: CardsId.JENNY_SLAYER, name: "Jenny Slayer", arrows: 0b00001000, delay: 5, base: Base["COMMON"], description: ":R: deal 3 damage. :n: When you play a £Jenny card: take 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //16
  JENNY_TIGER: {cardId: CardsId.JENNY_TIGER, name: "Jenny Tiger", arrows: 0b11000110, delay: 4, base: Base["UNCOMMON"], description: ":NC: :n: When active & you play a £Rotnip card: cage it & activate it & delay this card & deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  KEY: {cardId: CardsId.KEY, name: "Key", arrows: 0b10000000, delay: 1, base: Base["UNCOMMON"], description: ":R: activate a card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  KEY_X: {cardId: CardsId.KEY_X, name: "Key X", arrows: 0b00000000, delay: 2, base: Base["RARE"], description: "When you play a card: activate it & activate this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  KILLER_KARP: {cardId: CardsId.KILLER_KARP, name: "Killer Karp", arrows: 0b00010000, delay: 3, base: Base["UNCOMMON"], description: ":R: deal 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  KING_BUNI: {cardId: CardsId.KING_BUNI, name: "King Buni", arrows: 0b00101101, delay: 3, base: Base["UNCOMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  KNUCKLER: {cardId: CardsId.KNUCKLER, name: "Knuckler", arrows: 0b00010000, delay: 2, base: Base["COMMON"], description: ":R: deal 2 damage to the player with the most life.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  KROTERA: {cardId: CardsId.KROTERA, name: "Krotera", arrows: 0b10111010, delay: 3, base: Base["COMMON"], description: ":NC: :n: :R: deal 1 damage.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  LE_BIADLO: {cardId: CardsId.LE_BIADLO, name: "Le Biadlo", arrows: 0b01001001, delay: 2, base: Base["COMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //17
  LEAN_BOO: {cardId: CardsId.LEAN_BOO, name: "Lean Boo", arrows: 0b11111111, delay: 1, base: Base["UNCOMMON"], description: "When your opponent plays a card: cage it & silence it & activate it & remove this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  LEMON: {cardId: CardsId.LEMON, name: "Lemon", arrows: 0b01100010, delay: 2, base: Base["UNCOMMON"], description: "When active & you play a card: switch the colour of it.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  LENNY: {cardId: CardsId.LENNY, name: "Lenny", arrows: 0b10101001, delay: 3, base: Base["COMMON"], description: ":NC: :n: :R: delay all active cards.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  LIFE_PRESERVER: {cardId: CardsId.LIFE_PRESERVER, name: "Life Preserver", arrows: 0b01100010, delay: 3, base: Base["UNCOMMON"], description: ":R: gain shield.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  LIFE_PRESERVER_X: {cardId: CardsId.LIFE_PRESERVER_X, name: "Life Preserver X", arrows: 0b01100110, delay: 6, base: Base["UNCOMMON"], description: ":R: opponent gains shield.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  LONGBUN: {cardId: CardsId.LONGBUN, name: "Longbun", arrows: 0b10101010, delay: 2, base: Base["COMMON"], description: "", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  MAGIC_CRYSTAL: {cardId: CardsId.MAGIC_CRYSTAL, name: "Magic Crystal", arrows: 0b01010010, delay: 1, base: Base["COMMON"], description: ":R: gain 1 MANA token.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  MAMA_AND_SON: {cardId: CardsId.MAMA_AND_SON, name: "Mama and Son", arrows: 0b01010000, delay: 3, base: Base["COMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //18
  MANBUNJIN: {cardId: CardsId.MANBUNJIN, name: "Manbunjin", arrows: 0b00010001, delay: 5, base: Base["COMMON"], description: ":R: pop your opponent's shield. If you did: gain a POWER token.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  MAPMAN: {cardId: CardsId.MAPMAN, name: "Mapman", arrows: 0b10011010, delay: 3, base: Base["UNCOMMON"], description: ":P: tick one of your active cards. :n: :R: tick one of your active cards.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  MARIANNE: {cardId: CardsId.MARIANNE, name: "Marianne", arrows: 0b10001000, delay: 3, base: Base["COMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  MARK: {cardId: CardsId.MARK, name: "Mark", arrows: 0b10000111, delay: 2, base: Base["COMMON"], description: ":NC: :n: :R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  MASKED_JENNY_CAT: {cardId: CardsId.MASKED_JENNY_CAT, name: "Masked Jenny Cat", arrows: 0b11111111, delay: 2, base: Base["COMMON"], description: ":R: deal 1 damage for each connected card.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  MASKED_RUBY: {cardId: CardsId.MASKED_RUBY, name: "Masked Ruby", arrows: 0b10001001, delay: 2, base: Base["UNCOMMON"], description: ":R: gain shield & deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  MECHA_SANTA: {cardId: CardsId.MECHA_SANTA, name: "Mecha Santa", arrows: 0b00010011, delay: 2, base: Base["COMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  MECHA_SANTA_X: {cardId: CardsId.MECHA_SANTA_X, name: "Mecha Santa X", arrows: 0b00010011, delay: 2, base: Base["COMMON"], description: ":R: take 1 damage. :n: When you play a £Mecha card: deal 1 damage & switch the color of this card.", isBlue: true, isArrowBlue: false, status: Status.NONE,},

  //19
  METONYM: {cardId: CardsId.METONYM, name: "Metonym", arrows: 0b10101010, delay: 1, base: Base["RARE"], description: ":P: deal 2 damage. :n: :R: take 3 damage.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  MOOSE_MAN: {cardId: CardsId.MOOSE_MAN, name: "Moose Man", arrows: 0b01000000, delay: 1, base: Base["COMMON"], description: ":P: tick an active card. :n: :R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  MOTOR_MAN: {cardId: CardsId.MOTOR_MAN, name: "Motor Man", arrows: 0b10100100, delay: 2, base: Base["COMMON"], description: ":R: deal a damage for every 2 MANA tokens you have to the player with the most life.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  MR_X: {cardId: CardsId.MR_X, name: "Mr X", arrows: 0b10000010, delay: 3, base: Base["UNCOMMON"], description: ":P: silence one of your cards. :n: :R: deal 3 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  NEON: {cardId: CardsId.NEON, name: "Neon", arrows: 0b01011000, delay: 2, base: Base["COMMON"], description: ":NC: :n: :R: peek at your opponent's top 3 deck cards & peek at your top 3 deck cards.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  NETHERBIRD: {cardId: CardsId.NETHERBIRD, name: "Netherbird", arrows: 0b01110000, delay: 2, base: Base["UNCOMMON"], description: ":P: deal 1 damage. :n: :R: take 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  NILS: {cardId: CardsId.NILS, name: "Nils", arrows: 0b00000100, delay: 1, base: Base["UNCOMMON"], description: ":R: gain shield & deal 2 damage to the player with the most life.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  NUNA: {cardId: CardsId.NUNA, name: "Nuna", arrows: 0b01001000, delay: 2, base: Base["COMMON"], description: ":R: heal 1.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //20
  OCTACLE: {cardId: CardsId.OCTACLE, name: "Octacle", arrows: 0b10010011, delay: 2, base: Base["COMMON"], description: ":R: replace a random card in your opponent's hand with a £Sad £Trash card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  OH: {cardId: CardsId.OH, name: "Oh!", arrows: 0b00010001, delay: 3, base: Base["COMMON"], description: ":NC: :n: :R: heal 1.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  OLD_ITTLE: {cardId: CardsId.OLD_ITTLE, name: "Old Ittle", arrows: 0b00010000, delay: 4, base: Base["COMMON"], description: ":R: do one at random: deal 1 damage | deal 2 damage | deal 3 damage | take 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  OOGLER: {cardId: CardsId.OOGLER, name: "Oogler", arrows: 0b01011010, delay: 1, base: Base["COMMON"], description: ":R: delay an active card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  ORKA: {cardId: CardsId.ORKA, name: "Orka", arrows: 0b11100100, delay: 1, base: Base["UNCOMMON"], description: ":P: heal 1. :n: :R: deal 1 damage to the player with the most life.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  PARANOID_FISH: {cardId: CardsId.PARANOID_FISH, name: "Paranoid Fish", arrows: 0b01010101, delay: 2, base: Base["RARE"], description: "When you play a card: take 1 damage. :n: When your opponent plays a card: deal 1 damage.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  PARROT: {cardId: CardsId.PARROT, name: "Parrot", arrows: 0b00101011, delay: 3, base: Base["COMMON"], description: "", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  PARROT_X: {cardId: CardsId.PARROT_X, name: "Parrot X", arrows: 0b00101011, delay: 3, base: Base["UNCOMMON"], description: "When your opponent plays a card: transform this card into a copy of that card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //21
  PASSEL: {cardId: CardsId.PASSEL, name: "Passel", arrows: 0b00000000, delay: 1, base: Base["RARE"], description: "When you play a card: activate it & delay it twice & activate this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  PETAL_SLUG: {cardId: CardsId.PETAL_SLUG, name: "Petal Slug", arrows: 0b10001101, delay: 2, base: Base["COMMON"], description: ":NC: :n: :R: deal 1 piercing damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  PITCH_AND_CATSTRIKE: {cardId: CardsId.PITCH_AND_CATSTRIKE, name: "Pitch and Catstrike", arrows: 0b00011010, delay: 2, base: Base["UNCOMMON"], description: ":R: deal 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  PLASMA_HYDRA: {cardId: CardsId.PLASMA_HYDRA, name: "Plasma Hydra", arrows: 0b01010101, delay: 3, base: Base["UNCOMMON"], description: ":P: take 3 damage & deal 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  PORTAL_WAND: {cardId: CardsId.PORTAL_WAND, name: "Portal Wand", arrows: 0b00010001, delay: 2, base: Base["COMMON"], description: ":P: move a card to a random space.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  PORTAL_WORLD: {cardId: CardsId.PORTAL_WORLD, name: "Portal World", arrows: 0b11111111, delay: 1, base: Base["RARE"], description: ":U: :n: :P: return one of your cards.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  POTATO: {cardId: CardsId.POTATO, name: "Potato", arrows: 0b00000000, delay: 1, base: Base["UNCOMMON"], description: ":D: reveal this card & deal 1 damage & discard this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  PRIMAL_FISHBUN: {cardId: CardsId.PRIMAL_FISHBUN, name: "Primal Fishbun", arrows: 0b10001000, delay: 4, base: Base["UNCOMMON"], description: "When active & a £Bun card is played: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //22
  PRINCE_HINGST: {cardId: CardsId.PRINCE_HINGST, name: "Prince Hingst", arrows: 0b10101010, delay: 4, base: Base["UNCOMMON"], description: ":R: take 3 damage & take 2 damage & take 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  PRINCESS_REMEDY: {cardId: CardsId.PRINCESS_REMEDY, name: "Princess Remedy", arrows: 0b11000000, delay: 2, base: Base["UNCOMMON"], description: "When active & your opponent plays a card: heal 1.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  PSICLOPS: {cardId: CardsId.PSICLOPS, name: "Psiclops", arrows: 0b01000000, delay: 3, base: Base["COMMON"], description: "When active & a card is played: deal 1 damage to the player with the most life.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  PUZZLE: {cardId: CardsId.PUZZLE, name: "Puzzle", arrows: 0b00101100, delay: 2, base: Base["COMMON"], description: ":P: delay all opponent active cards.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  QUEEN: {cardId: CardsId.QUEEN, name: "Queen", arrows: 0b10100000, delay: 3, base: Base["RARE"], description: "When you play a £King card: remove a card & activate this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  QUEEN_AMELIA: {cardId: CardsId.QUEEN_AMELIA, name: "Queen Amelia", arrows: 0b10100001, delay: 3, base: Base["RARE"], description: ":R: remove one of your cards & gain 2 MANA tokens.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  QUEEN_AMELIA_X: {cardId: CardsId.QUEEN_AMELIA_X, name: "Queen Amelia X", arrows: 0b10100001, delay: 3, base: Base["RARE"], description: ":R: remove a card & remove 2 MANA tokens.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  RAFT: {cardId: CardsId.RAFT, name: "Raft", arrows: 0b11011101, delay: 3, base: Base["UNCOMMON"], description: "", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //23
  RAINBOW_RING: {cardId: CardsId.RAINBOW_RING, name: "Rainbow Ring", arrows: 0b00000010, delay: 2, base: Base["RARE"], description: ":U: :n: When you play a card: add a copy of that card to the top of your deck & remove this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  RANDO: {cardId: CardsId.RANDO, name: "Rando", arrows: 0b00100101, delay: 2, base: Base["COMMON"], description: ":U: :n: :R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  RAZZLE_DAZZLE: {cardId: CardsId.RAZZLE_DAZZLE, name: "Razzle Dazzle", arrows: 0b00111001, delay: 2, base: Base["UNCOMMON"], description: ":NC: :n: :R: silence a card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  REIKA: {cardId: CardsId.REIKA, name: "Reika", arrows: 0b11011001, delay: 1, base: Base["UNCOMMON"], description: ":R: deal 1 damage & move a card to a random space.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  ROLE_REVERSAL: {cardId: CardsId.ROLE_REVERSAL, name: "Role Reversal", arrows: 0b10001000, delay: 1, base: Base["RARE"], description: ":R: switch hands with the opponent.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  ROLL: {cardId: CardsId.ROLL, name: "Roll", arrows: 0b10001000, delay: 1, base: Base["COMMON"], description: ":R: gain shield.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  ROTNIP: {cardId: CardsId.ROTNIP, name: "Rotnip", arrows: 0b10010110, delay: 2, base: Base["COMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SAFETY_JENNY: {cardId: CardsId.SAFETY_JENNY, name: "Safety Jenny", arrows: 0b10011001, delay: 3, base: Base["COMMON"], description: "", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //24
  SAM: {cardId: CardsId.SAM, name: "Sam", arrows: 0b00101000, delay: 2, base: Base["COMMON"], description: ":P: pay a MANA token to deal 1 damage. :n: :R: peek at your opponent's top 3 deck cards.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SATURNUS: {cardId: CardsId.SATURNUS, name: "Saturnus", arrows: 0b00000010, delay: 2, base: Base["COMMON"], description: ":P: heal 1. :n: :R: peek at your top 3 deck cards.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SHELLBUN: {cardId: CardsId.SHELLBUN, name: "Shellbun", arrows: 0b00100010, delay: 2, base: Base["COMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SHOP_AT_ITAN_S: {cardId: CardsId.SHOP_AT_ITAN_S, name: "Shop at Itan's", arrows: 0b01001001, delay: 2, base: Base["UNCOMMON"], description: ":R: add a £Portal £Wand card to your deck & add a £Ice £Wand card to your deck & add a £Force £Wand card to your deck.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SHORTCUT: {cardId: CardsId.SHORTCUT, name: "Shortcut", arrows: 0b01000111, delay: 2, base: Base["COMMON"], description: ":R: pop your opponent's shield. If you did: deal 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SIMULACRUM: {cardId: CardsId.SIMULACRUM, name: "Simulacrum", arrows: 0b11111000, delay: 3, base: Base["RARE"], description: "When active: Whenever you deal 2 damage; deal 3 damage instead.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SIMULACRUM_X: {cardId: CardsId.SIMULACRUM_X, name: "Simulacrum X", arrows: 0b11111000, delay: 4, base: Base["RARE"], description: "When active: Whenever you deal 3 damage to yourself; gain shield instead.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SKULL_CARD: {cardId: CardsId.SKULL_CARD, name: "Skull Card", arrows: 0b00001000, delay: 1, base: Base["UNCOMMON"], description: ":P: take 2 damage. :n: :R: remove a card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //25
  SKULL_CARD_X: {cardId: CardsId.SKULL_CARD_X, name: "Skull Card X", arrows: 0b11111111, delay: 3, base: Base["COMMON"], description: "When active & you play a card: take 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SKULL_KING: {cardId: CardsId.SKULL_KING, name: "Skull King", arrows: 0b00000001, delay: 4, base: Base["UNCOMMON"], description: ":R: deal 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SKULLNIP: {cardId: CardsId.SKULLNIP, name: "Skullnip", arrows: 0b00111100, delay: 2, base: Base["COMMON"], description: ":R: deal 1 damage for each connected turnip card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SLIME: {cardId: CardsId.SLIME, name: "Slime", arrows: 0b10000000, delay: 3, base: Base["COMMON"], description: ":R: heal 1.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SLUG: {cardId: CardsId.SLUG, name: "Slug", arrows: 0b10101010, delay: 2, base: Base["RARE"], description: ":R: transform an opponent active card into µSlug.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SNAP: {cardId: CardsId.SNAP, name: "Snap", arrows: 0b01001011, delay: 1, base: Base["COMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SOFIA: {cardId: CardsId.SOFIA, name: "Sofia", arrows: 0b00110001, delay: 2, base: Base["UNCOMMON"], description: ":P: cage a common card. :n: :R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SPACE_NORD: {cardId: CardsId.SPACE_NORD, name: "Space Nörd", arrows: 0b01101001, delay: 3, base: Base["UNCOMMON"], description: "When active: Whenever you would shield your opponent; shield yourself instead.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //26
  SPACE_NORD_X: {cardId: CardsId.SPACE_NORD_X, name: "Space Nörd X", arrows: 0b11101001, delay: 5, base: Base["RARE"], description: "When active: Whenever you would shield yourself; shield your opponent instead.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SPECTRE: {cardId: CardsId.SPECTRE, name: "Spectre", arrows: 0b01101111, delay: 1, base: Base["RARE"], description: ":R: gain 1 MANA token. :n: When you play a card: pay a MANA token to deal 1 damage.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  SPIKE: {cardId: CardsId.SPIKE, name: "Spike", arrows: 0b01010101, delay: 1, base: Base["COMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  SPIKE_X: {cardId: CardsId.SPIKE_X, name: "Spike X", arrows: 0b01010101, delay: 2, base: Base["COMMON"], description: ":NC: :n: :R: deal 1 piercing damage.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  SPIKEBUN: {cardId: CardsId.SPIKEBUN, name: "Spikebun", arrows: 0b00001011, delay: 3, base: Base["COMMON"], description: "When active: Whenever you would shield yourself; deal 2 damage instead.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  STAR_SPLITTER: {cardId: CardsId.STAR_SPLITTER, name: "Star Splitter", arrows: 0b10001000, delay: 2, base: Base["COMMON"], description: ":P: delay an opponent active card. :n: :R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  STICK: {cardId: CardsId.STICK, name: "Stick", arrows: 0b01000100, delay: 3, base: Base["COMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  STRANGLE_SWAN: {cardId: CardsId.STRANGLE_SWAN, name: "Strangle Swan", arrows: 0b01111100, delay: 2, base: Base["COMMON"], description: ":NC: :n: :R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //27
  STUPID_BEE: {cardId: CardsId.STUPID_BEE, name: "Stupid Bee", arrows: 0b10110000, delay: 2, base: Base["COMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SUPER_ANN_ZEIGER: {cardId: CardsId.SUPER_ANN_ZEIGER, name: "Super Ann Zeiger", arrows: 0b10010001, delay: 3, base: Base["UNCOMMON"], description: ":R: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SWIMMY_ROGER: {cardId: CardsId.SWIMMY_ROGER, name: "Swimmy Roger", arrows: 0b00101010, delay: 2, base: Base["COMMON"], description: ":R: deal 1 damage or take 1 damage at random - three times.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  THAT_GUY: {cardId: CardsId.THAT_GUY, name: "That Guy", arrows: 0b01010101, delay: 1, base: Base["UNCOMMON"], description: ":P: the player with the least life heals 2. :n: :R: take 3 damage.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  THAT_GUY_WITH_HANDS: {cardId: CardsId.THAT_GUY_WITH_HANDS, name: "That Guy With Hands", arrows: 0b10010000, delay: 3, base: Base["RARE"], description: "When active & you play a card: switch the color of all other cards.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  THE_BIGGEST_FAN: {cardId: CardsId.THE_BIGGEST_FAN, name: "The Biggest Fan", arrows: 0b11111111, delay: 6, base: Base["COMMON"], description: ":NC: :n: :P: activate this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  THE_KING: {cardId: CardsId.THE_KING, name: "The King", arrows: 0b00001010, delay: 3, base: Base["RARE"], description: "When you play a £Queen card: activate all cards.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  THE_LICHIOUS_TURNIP: {cardId: CardsId.THE_LICHIOUS_TURNIP, name: "The Lichious Turnip", arrows: 0b00110011, delay: 2, base: Base["UNCOMMON"], description: ":R: transform one of your cards into µRotnip & transform an opponent card into µRotnip.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //28
  TIPPSIE: {cardId: CardsId.TIPPSIE, name: "Tippsie", arrows: 0b00100100, delay: 5, base: Base["COMMON"], description: ":NC: :n: :R: heal 2.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  TIPPSIE_TOSS: {cardId: CardsId.TIPPSIE_TOSS, name: "Tippsie Toss", arrows: 0b10000011, delay: 3, base: Base["UNCOMMON"], description: "When active & you play a £Tippsie card: deal 1 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  TITAN: {cardId: CardsId.TITAN, name: "Titan", arrows: 0b00010110, delay: 2, base: Base["COMMON"], description: "When your opponent plays a card: move this card to a random space.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  TOR: {cardId: CardsId.TOR, name: "Tor", arrows: 0b01000000, delay: 3, base: Base["UNCOMMON"], description: ":P: remove one of your cards. :n: :R: deal 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  TOSCA: {cardId: CardsId.TOSCA, name: "Tosca", arrows: 0b00000000, delay: 2, base: Base["RARE"], description: ":P: add 2 £Key cards to your deck. :n: :R: heal 3.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  TOSCA_X: {cardId: CardsId.TOSCA_X, name: "Tosca X", arrows: 0b00000000, delay: 3, base: Base["RARE"], description: ":P: activate this card. :n: When you play a card: cage it & gain shield.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  TURNIP: {cardId: CardsId.TURNIP, name: "Turnip", arrows: 0b01000110, delay: 3, base: Base["COMMON"], description: ":R: add a £Rotnip card to the top of your deck.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  ULTRA_FISHBUNJIN_3000: {cardId: CardsId.ULTRA_FISHBUNJIN_3000, name: "Ultra Fishbunjin 3000", arrows: 0b01001010, delay: 10, base: Base["RARE"], description: ":NC: :n: :R: deal 4 damage. :n: When active & you play a common card: tick this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  //29
  ULTRA_MECHABUN: {cardId: CardsId.ULTRA_MECHABUN, name: "Ultra Mechabun", arrows: 0b01010101, delay: 3, base: Base["RARE"], description: ":R: deal 2 damage. :n: When active & you play a card: transform it into µFishbun.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  UR: {cardId: CardsId.UR, name: "Ur", arrows: 0b01110111, delay: 1, base: Base["UNCOMMON"], description: ":R: peek at your opponent's hand.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  VAL_ENBERG: {cardId: CardsId.VAL_ENBERG, name: "Val Enberg", arrows: 0b10100100, delay: 2, base: Base["COMMON"], description: ":P: tick an opponent active card twice.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  VALENTINE_LENNY: {cardId: CardsId.VALENTINE_LENNY, name: "Valentine Lenny", arrows: 0b00000000, delay: 1, base: Base["RARE"], description: ":R: the player with the least life heals 5.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  VOLCANO: {cardId: CardsId.VOLCANO, name: "Volcano", arrows: 0b00000100, delay: 4, base: Base["UNCOMMON"], description: ":P: silence an adjacent card at random.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  WASH_OLOF: {cardId: CardsId.WASH_OLOF, name: "Wash-Olof", arrows: 0b00000000, delay: 2, base: Base["COMMON"], description: ":R: deal 3 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  YOUNG_LICHIOUS_TURNIP: {cardId: CardsId.YOUNG_LICHIOUS_TURNIP, name: "Young Lichious Turnip", arrows: 0b00000000, delay: 2, base: Base["RARE"], description: ":P: activate this card. :n: When your opponent plays a card: gain 1 MANA token.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  Z: {cardId: CardsId.Z, name: "Z", arrows: 0b10000010, delay: 3, base: Base["UNCOMMON"], description: ":P: take 1 damage. :n: :R: deal 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  // Unobtainable (Not available in deck editing)
  A_LEMON: {cardId: CardsId.A_LEMON, name: "A Lemon", arrows: 0b10101010, delay: 2, base: Base["UNCOMMON"], description: ":R: silence a card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  ANCESTRAL_DUCKLORD: {cardId: CardsId.ANCESTRAL_DUCKLORD, name: "Ancestral Ducklord", arrows: 0b10010110, delay: 3, base: Base["UNCOMMON"], description: ":R: remove a card. :n: When active & your opponent plays a card: deal 1 piercing damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  BUNICORN: {cardId: CardsId.BUNICORN, name: "Bunicorn", arrows: 0b00101001, delay: 1, base: Base["COMMON"], description: ":U: :n: :R: deal 2 piercing damage & add a £Heart card to your deck.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  DEATH_THROE: {cardId: CardsId.DEATH_THROE, name: "Death Throe", arrows: 0b10101010, delay: 3, base: Base["RARE"], description: "When active & a card is played: heal 1 & gain shield.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  LEMON_COCKTAIL: {cardId: CardsId.LEMON_COCKTAIL, name: "Lemon Cocktail", arrows: 0b00000110, delay: 1, base: Base["UNCOMMON"], description: ":R: restore a card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  LEMON_JUICE: {cardId: CardsId.LEMON_JUICE, name: "Lemon Juice", arrows: 0b01100000, delay: 1, base: Base["UNCOMMON"], description: ":R: deal 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  LEMON_TART: {cardId: CardsId.LEMON_TART, name: "Lemon Tart", arrows: 0b11000000, delay: 2, base: Base["UNCOMMON"], description: ":R: heal 1.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  LEMONS: {cardId: CardsId.LEMONS, name: "Lemons", arrows: 0b00000000, delay: 1, base: Base["RARE"], description: ":U: :n: :P: activate this card. :n: :R: make lemonade.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SLICED_LEMON: {cardId: CardsId.SLICED_LEMON, name: "Sliced Lemon", arrows: 0b00001100, delay: 2, base: Base["UNCOMMON"], description: ":R: deal 2 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},

  // Tokens
  T_BIRD: {cardId: CardsId.T_BIRD, name: "Bird", arrows: 0b00010000, delay: 2, base: Base["TOKEN"], description: "", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  T_BLUEPRINT: {cardId: CardsId.T_BLUEPRINT, name: "Blueprint", arrows: 0b10101010, delay: 3, base: Base["TOKEN"], description: ":P: activate this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  FATIGUE: {cardId: CardsId.FATIGUE, name: "Fatigue", arrows: 0b11111111, delay: 2, base: Base["TOKEN"], description: ":D: reveal this card & take fatigue damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  FISHBUN_PILEUP: {cardId: CardsId.FISHBUN_PILEUP, name: "Fishbun Pileup", arrows: 0b00000000, delay: 3, base: Base["TOKEN"], description: ":R: deal 3 damage.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  T_HORSEGUN: {cardId: CardsId.T_HORSEGUN, name: "Horsegun", arrows: 0b10110001, delay: 2, base: Base["TOKEN"], description: "When you play a £Horse card: deal 1 damage.", isBlue: true, isArrowBlue: false, status: Status.NONE,},
  NEO_DEA: {cardId: CardsId.NEO_DEA, name: "Neo Dea", arrows: 0b01001010, delay: 3, base: Base["TOKEN"], description: ":R: heal 2.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
  SAD_TRASH: {cardId: CardsId.SAD_TRASH, name: "Sad Trash", arrows: 0b00000000, delay: 1, base: Base["TOKEN"], description: ":P: discard a card from you hand & activate this card.", isBlue: true, isArrowBlue: true, status: Status.NONE,},
};

export default Cards;
